import UIKit
import CoreData
import CloudKit


extension UIApplication {
    static var viewContext: NSManagedObjectContext {
        (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        let container = NSPersistentCloudKitContainer(name: "ESafe")
        container.loadPersistentStores(completionHandler: { storeDescription, _ in print(storeDescription)})
        try? container.viewContext.setQueryGenerationFrom(.current)
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return container
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        persistentContainer.setupLocalStoreIfNeeded()
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if UIApplication.viewContext.hasChanges { try! UIApplication.viewContext.save() }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if UIApplication.viewContext.hasChanges { try! UIApplication.viewContext.save() }
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
}

