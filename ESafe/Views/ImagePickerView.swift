import SwiftUI


class ImagePickerVCDelegate: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    let pickerView: ImagePickerView
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let selectedImage = info[.originalImage] {
            pickerView.selectedImage = pickerView.newSize == nil ? selectedImage as! UIImage: (selectedImage as! UIImage).resize(to: pickerView.newSize!)
        }
        pickerView.presentationMode.wrappedValue.dismiss()
    }
    
    init(picker: ImagePickerView) {
        self.pickerView = picker
    }
    
}


struct ImagePickerView: UIViewControllerRepresentable {
    @Environment(\.presentationMode) var presentationMode
    @Binding var selectedImage: UIImage
    let newSize: CGSize?
    
    func makeCoordinator() -> ImagePickerVCDelegate {
        ImagePickerVCDelegate(picker: self)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePickerView>) -> UIImagePickerController {
        let pickerVC = UIImagePickerController()
        pickerVC.delegate = context.coordinator
        return pickerVC
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<ImagePickerView>) {
    }
    
}
