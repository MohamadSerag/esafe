import SwiftUI
import CoreData


struct NesabSegmentView: View {
    @ObservedObject var viewModel: NesabSegmentVM
    
    var body: some View {
        Section(header: Text("Nesab Base"), footer: Text("EGP\(viewModel.nesabInCash, specifier: "%g")")) {
            Picker("", selection: $viewModel.nesabSelectionIndex) {
                ForEach(0 ..< viewModel.nesabs.count, id: \.self) {
                    Text(self.viewModel.nesabs[$0].name)
                }
            }.pickerStyle(SegmentedPickerStyle())
        }
    }
    
}


class NesabSegmentVM: ObservableObject {
    @Published var nesabSelectionIndex: Int { didSet { update() } }
    @Published var nesabInCash: Double
    let nesabs: [NesabModel]
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext) {
        nesabs = moc.fetchNesabs()
        let activeNesab =  moc.fetchNesabs().first { $0.isActive }!
        nesabSelectionIndex = moc.fetchNesabs().firstIndex(of: activeNesab)!
        nesabInCash = (activeNesab.grams * activeNesab.karat.egpRate).rounded()
    }
    
    private func update() {
        nesabs.forEach { $0.isActive.toggle() }
        nesabInCash = (nesabs[nesabSelectionIndex].grams * nesabs[nesabSelectionIndex].karat.egpRate).rounded()
    }
    
}
