import SwiftUI
import QGrid


struct KaratDetailsForm: View {
    @ObservedObject var viewModel: AbstractKaratDetailsVM
    @Environment(\.presentationMode) var presentationMode
    
    var title = ""
    
    private var validUserInput: Bool { viewModel.name.count > 0 && Double(viewModel.egpRate) != nil }
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Name", text: $viewModel.name)
                }
                Section {
                    TextField("EGP Rate", text: $viewModel.egpRate).keyboardType(.decimalPad)
                }
            }
            .navigationBarTitle("\(title)", displayMode: .inline)
            .navigationBarItems(trailing: Button("Save") { self.saveButtonTapped() }.disabled(!validUserInput))
        }
    }
    
    private func saveButtonTapped() {
        viewModel.save()
        presentationMode.wrappedValue.dismiss()
    }
    
}

