import SwiftUI


struct KaratCell: View {
    @ObservedObject var karat: KaratModel
    let currencySymbol: String
    
    @State private var showingEditView = false
    
    var body: some View {
        HStack {
            Text(karat.name).foregroundColor(.primary)
            Spacer()
            Text("\(currencySymbol)\(karat.egpRate, specifier: "%g")")
                .applyEGPTotalModifiers()
        }
        .contextMenu { ContextMenuEditButton(showingEditView: $showingEditView) }
        .sheet(isPresented: $showingEditView) { KaratDetailsForm(viewModel: EditKaratVM(itemToEdit: self.karat), title: "Edit Karat") }
    }
    
}
