import CoreData
import SwiftUI


class AbstractKaratDetailsVM: ObservableObject {
    @Published var name = ""
    @Published var egpRate = ""
    
    var userInput: [String: Any] { ["name": name, "egpRate": Double(egpRate)!] }
    let moc: NSManagedObjectContext
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext) {
        self.moc = moc
    }
    
    func save() {
        fatalError("Implement This!")
    }
    
    func delete() { }
    
}


class AddKaratVM: AbstractKaratDetailsVM {
    override func save() {
        moc.createKarat(with: userInput)
    }
    
}


class EditKaratVM: AbstractKaratDetailsVM {
    var itemToEdit: KaratModel!
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext, itemToEdit: KaratModel) {
        self.itemToEdit = itemToEdit
        super.init(moc: moc)
        name = itemToEdit.name
        egpRate = String(format: "%g", itemToEdit.egpRate)
    }
    
    override func save() {
        moc.update(itemToEdit, with: userInput)
    }
    
    override func delete() {
        moc.delete(itemToEdit)
    }
    
}
