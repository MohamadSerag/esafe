import SwiftUI


struct LocationCell: View {
    @ObservedObject var loc: CashLocationModel
    @State private var showingEditView = false
    
    var body: some View {
        HStack {
            Text(loc.name)
                .contextMenu { ContextMenuEditButton(showingEditView: $showingEditView) }
        }
        .sheet(isPresented: $showingEditView) { LocationDetailsForm(viewModel: EditLocationVM(itemToEdit: self.loc), title: "Edit Location") }
    }
    
}
