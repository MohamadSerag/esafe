import SwiftUI


struct LocationDetailsForm: View {
    @ObservedObject var viewModel: AbstractLocationDetailsVM
    @Environment(\.presentationMode) var presentationMode
    
    var title = ""
    
    private var validUserInput: Bool { viewModel.name.count > 0 }
    
    var body: some View {
        NavigationView {
            Form {
                TextField("Name", text: $viewModel.name)
            }
            .navigationBarTitle("\(title)", displayMode: .inline)
            .navigationBarItems(trailing: Button("Save") { self.saveButtonTapped() }.disabled(!validUserInput))
        }
    }
    
    private func saveButtonTapped() {
        viewModel.save()
        presentationMode.wrappedValue.dismiss()
    }
    
}
