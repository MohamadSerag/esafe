import SwiftUI
import CoreData


class AbstractLocationDetailsVM: ObservableObject {
    @Published var name = ""
    
    var userInput: [String: Any] { ["name": name] }
    let moc: NSManagedObjectContext
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext) {
        self.moc = moc
    }
    
    func save() {
        fatalError("Implement This!")
    }
    
    func delete() {
        fatalError("Implement This!")
    }
    
}


class AddLocationVM: AbstractLocationDetailsVM {
    
    override func save() {
        moc.createLocation(with: userInput)
    }
    
}


class EditLocationVM: AbstractLocationDetailsVM {
    var itemToEdit: CashLocationModel
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext, itemToEdit: CashLocationModel) {
        self.itemToEdit = itemToEdit
        super.init(moc: moc)
        name = itemToEdit.name
    }
    
    override func save() {
        moc.update(itemToEdit, with: userInput)
    }
    
    override func delete() {
        moc.delete(itemToEdit)
    }
    
}
