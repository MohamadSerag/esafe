import CoreData
import SwiftUI


class AbstractCurrencyVM: ObservableObject {
    @Published var code = ""
    @Published var symbol = ""
    @Published var egpRate = ""
    @Published var selectedColorName = CashCurrencyModel.availableColor.sortedKeys.first!
    
    var colorSelectionNames: [String] = { CashCurrencyModel.availableColor.sortedKeys }()
    var userInput: [String: Any] {
        [
            "code": code,
            "symbol": symbol,
            "egpRate": Double(egpRate)!,
            "colorName": selectedColorName
        ]
    }
    let moc: NSManagedObjectContext
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext) {
        self.moc = moc
    }
    
    func color(for name: String) -> Color {
        CashCurrencyModel.availableColor[name]!
    }
    
    func save() {
        fatalError("Implement This!")
    }
    
    func delete() {
        fatalError("Implement This!")
    }
    
}


class AddCurrencyVM: AbstractCurrencyVM {
    
    override func save() {
        moc.createCurrency(with: userInput)
    }
    
}


class EditCurrencyVM: AbstractCurrencyVM {
    var itemToEdit: CashCurrencyModel
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext, itemToEdit: CashCurrencyModel) {
        self.itemToEdit = itemToEdit
        super.init(moc: moc)
        code = itemToEdit.code
        symbol = itemToEdit.symbol
        egpRate = String(format: "%g", itemToEdit.egpRate)
        selectedColorName = itemToEdit.colorName
    }
    
    override func save() {
        moc.update(itemToEdit, with: userInput)
    }
    
    override func delete() {
        moc.delete(itemToEdit)
    }
    
}


