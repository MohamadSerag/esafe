import SwiftUI
import QGrid


struct CurrencyDetailsForm: View {
    @ObservedObject var viewModel: AbstractCurrencyVM
    @Environment(\.presentationMode) var presentationMode
    
    var title = ""
    
    private var validUserInput: Bool {
        viewModel.code.count > 0 && viewModel.symbol.count > 0 && Double(viewModel.egpRate) != nil
    }
    
    var body: some View {
        NavigationView {
            VStack(alignment: .center) {
                ZStack {
                    Circle()
                        .frame(width: 70, height: 70, alignment: .center)
                        .foregroundColor(Color(viewModel.selectedColorName))
                    Text(viewModel.code)
                        .foregroundColor(Color(#colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)))
                }
                
                TextField("Code", text: $viewModel.code)
                TextField("Symbol", text: $viewModel.symbol)
                TextField("EGP Rate", text: $viewModel.egpRate).keyboardType(.decimalPad)
                
                ColorGrid(viewModel: viewModel)
            }
            .padding()
            .navigationBarTitle("\(title)", displayMode: .inline)
            .navigationBarItems(trailing: Button("Save") { self.saveButtonTapped() }.disabled(!validUserInput))
        }
    }
    
    private func saveButtonTapped() {
        viewModel.save()
        presentationMode.wrappedValue.dismiss()
    }
    
}


struct ColorGrid: View {
    let viewModel: AbstractCurrencyVM
    let columnCount = 3
    
    var body: some View {
            QGrid(viewModel.colorSelectionNames, columns: columnCount) { colorName in
            Circle()
                .onTapGesture { self.viewModel.selectedColorName = colorName}
                .frame(width: 65, height: 65, alignment: .center)
                .foregroundColor(self.viewModel.color(for: colorName))
        }
    }
    
}
