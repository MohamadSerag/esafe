import SwiftUI


struct CurrencyCell: View {
    @ObservedObject var currency: CashCurrencyModel
    let currencySymbol: String
    
    @State private var showingEditView = false
    
    var body: some View {
        HStack(alignment: .center) {
            VStack(alignment: .leading) {
                Text(currency.code)
                Text(currency.symbol)
                    .font(.subheadline)
            }
            Spacer()
            Text("\(currencySymbol)\(currency.egpRate, specifier: "%g")")
                .applyEGPTotalModifiers()
        }
        .contextMenu { ContextMenuEditButton(showingEditView: $showingEditView) }
        .sheet(isPresented: $showingEditView) { CurrencyDetailsForm(viewModel: EditCurrencyVM(itemToEdit: self.currency), title: "Edit Currency") }
    }
    
}
