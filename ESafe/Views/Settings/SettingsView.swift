import SwiftUI


struct SettingsView: View {
    var karatListView: some View {
        let form = KaratDetailsForm(viewModel: AddKaratVM(), title: "Add Karat")
        return SettingItemListView(
            addItemView: form.eraseToAnyView(),
            fetchRequest: FetchRequest(fetchRequest: KaratModel.sortedFetchRequest()),
            title: "Karats"
        )
    }
    var locationListView: some View {
        let form = LocationDetailsForm(viewModel: AddLocationVM(), title: "Add Location")
        return SettingItemListView(
            addItemView: form.eraseToAnyView(),
            fetchRequest: FetchRequest(fetchRequest: CashLocationModel.sortedFetchRequest()),
            title: "Locations"
        )
    }
    var currencyListView: some View {
        let form = CurrencyDetailsForm(viewModel: AddCurrencyVM(), title: "Add Currency")
        return SettingItemListView(
            addItemView: form.eraseToAnyView(),
            fetchRequest: FetchRequest(fetchRequest: CashCurrencyModel.sortedFetchRequest()),
            title: "Currencies"
        )
    }
   
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Jewelry")) {
                    NavigationLink(destination: karatListView) {
                        HStack {
                            Text("Karat")
                        }
                    }
                }
                Section(header: Text("Cash")) {
                    NavigationLink(destination: currencyListView) {
                        HStack {
                            Text("Currency")
                        }
                    }
                    NavigationLink(destination: locationListView) {
                        HStack {
                            Text("Location")
                        }
                    }
                }
                
                NesabSegmentView(viewModel: NesabSegmentVM())
                
                Section {
                    HStack {
                        Text("Version")
                        Spacer()
                        Text("\(Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)")
                    }
                    .foregroundColor(.secondary)
                }
            }  
            .navigationBarTitle("Settings")
        }
    }
    
}
