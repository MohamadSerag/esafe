import SwiftUI
import CoreData


struct SettingItemListView<ItemType: NSManagedObject & ListItem>: View {
    let addItemView: AnyView
    let fetchRequest: FetchRequest<ItemType>
    let title: String
    
    @Environment(\.managedObjectContext) var moc
    @Environment(\.editMode) var editMode
    
    @State private var showingAddView = false
    @State private var showingAlert = false
    @State private var alert: Alert?
    
    private var items: FetchedResults<ItemType> { fetchRequest.wrappedValue }
    private var trailingButtons: some View {
        HStack(spacing: 18) {
            Button(action: { self.showingAddView.toggle() }, label: { plusLabel })
                .disabled(editMode!.wrappedValue.isEditing)
            EditButton()
        }
    }
    
    var body: some View {
        List {
            ForEach(items, id: \.objectID) { item in
                CellMaker.make(type: ItemType.self, info: Dictionary(dictionaryLiteral: ("item", item), ("currencySymbol", self.moc.defaultCurrencySymbol)))
            }
            .onDelete { self.deleteTapped(item: self.items[$0.first!]) }
        }
        .navigationBarTitle(Text(title), displayMode: .inline)
        .navigationBarItems(trailing: trailingButtons)
        .sheet(isPresented: $showingAddView) { self.addItemView }
        .alert(isPresented: $showingAlert) { alert! }
    }
    
    private func deleteTapped(item: NSManagedObject & ListItem) {
        
        switch item.removabilityState {
        case .removable:
            moc.delete(item)
            
        case .removableWithCascade:
            let deleteButton = Alert.Button.destructive(Text("Delete")) { self.moc.delete(item) }
            alert = Alert(
                title: Text("Confirmation"),
                message: Text("If you delete this item, all related items will be delete!"),
                primaryButton: .cancel(),
                secondaryButton: deleteButton
            )
            showingAlert.toggle()
            
        case .unremovable:
            alert = Alert(
                title: Text("Info"),
                message: Text("Only items added by user can be deleted."),
                dismissButton: .default(Text("Ok"))
            )
            showingAlert.toggle()
        }
    }
    
}


class CellMaker {
    class func make(type: Any, info: [String:Any]) -> some View {
        
        switch type {
        case is KaratModel.Type:
            return KaratCell(
                karat: info["item"]! as! KaratModel, currencySymbol: info["currencySymbol"]! as! String
            ).eraseToAnyView()
            
        case is CashLocationModel.Type:
            return LocationCell(loc: info["item"]! as! CashLocationModel).eraseToAnyView()
            
        case is CashCurrencyModel.Type:
            return CurrencyCell(
                currency:info["item"]! as! CashCurrencyModel, currencySymbol: info["currencySymbol"]! as! String
            ).eraseToAnyView()
            
        default:
            return Text("Unknow Input").eraseToAnyView()
        }
    }
    
}
