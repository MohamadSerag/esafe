import SwiftUI


struct OwnerListView: View {
    @FetchRequest(fetchRequest: OwnerModel.sortedFetchRequest()) var owners: FetchedResults<OwnerModel>
    
    @State private var showingAddView = false
    private var addLabel: some View { Image(systemName: "person.crop.circle.badge.plus").imageScale(.large) }
    
    var body: some View {
        NavigationView {
            ScrollView {
                if owners.count == 0 {
                    placeholderView(title: "No Owners Yet!", subTitle: "Tap the add button up right.")
                        .offset(x: 0, y: UIScreen.main.bounds.height / 3.5)
                } else {
                    ForEach(owners) { owner in
                        VStack {
                            OwnerCell(owner: owner).padding()
                        }
                    }
                }
            }
            .frame(maxWidth: .infinity)
            .navigationBarTitle("Owners".localized())
            .navigationBarItems(trailing: Button(action: { self.showingAddView.toggle() }, label: { addLabel }))
        }
        .sheet(isPresented: $showingAddView) { OwnerDetailsForm(showingDeleteButton: false) }
    }
    
}


struct OwnerCell: View {
    @ObservedObject var owner: OwnerModel
    @Environment(\.managedObjectContext) var moc
    
    @State private var showingEditView = false
    @State private var showingZakahAlert = false
    private var cashListView: some View {
        let vm = CashItemListVM(owner: owner)
        return CashItemListView(viewModel: vm)
    }
    private var goldListView: some View {
        let vm = GoldItemListVM(owner: owner)
        return GoldItemListView(viewModel: vm)
    }
    
    var body: some View {
        HStack(spacing: 30) {
            
            Image(uiImage: UIImage(data: owner.photo)!)
                .applyOwnerModifiers()
            
            VStack(alignment: .leading) {
                Text(owner.name).font(.title)
                
                // buttons row
                HStack(spacing: 15) {
                    NavigationLink(destination: goldListView) { Image("gold.filled").renderingMode(.template) }
                    NavigationLink(destination: cashListView) { Image("cash.filled").renderingMode(.template) }
                    Button(action: { self.showingZakahAlert.toggle() }) { Image("zakah.filled").renderingMode(.template) }
                    Button(action: { self.showingEditView.toggle() }) { Image("edit.owner.filled").renderingMode(.template) }
                }
            }
            
            Spacer()
        }
        .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
        .sheet(isPresented: $showingEditView) { OwnerDetailsForm(viewModel: EditOwnerViewModel(owner: self.owner)) }
        .alert(isPresented: $showingZakahAlert) { zakahAlert(for: owner) }
    }
    
    private func zakahAlert(for owner: OwnerModel) -> Alert {
        let amount = ZakahCalculator(moc: moc).zakah(for: owner)
        return Alert(title: Text("Zakah"),
              message: Text("All items Zakah: \(moc.defaultCurrencySymbol)\(amount, specifier: "%g")"),
              dismissButton: .default(Text("OK")))
    }
    
}
