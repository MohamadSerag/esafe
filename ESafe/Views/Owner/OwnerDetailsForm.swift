import SwiftUI


struct OwnerDetailsForm: View {
    @ObservedObject var viewModel: AbstractOwnerDetailsViewModel = AddOwnerViewModel()
    @Environment(\.presentationMode) var presentationMode
    var showingDeleteButton = true
    
    @State private var showingImagePicker = false
    @State private var showingDeletionConfirmation = false
    @State private var deletionAlert: Alert?
    private var validUserInput: Bool { viewModel.name.count > 0 }
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Button("Save".localized()) { self.saveButtonTapped() }
                    .disabled(!validUserInput)
                    .padding([.leading, .top, .trailing])
            }
            HStack(alignment: .center) {
                Spacer()
                Image(uiImage: viewModel.photo)
                    .applyOwnerModifiers(width: 130, height: 130)
                    .onTapGesture { self.showingImagePicker.toggle() }
                Spacer()
            }
            .padding(.bottom)
            
            Form {
                Section {
                    TextField("Name".localized(), text: $viewModel.name)
                }
                
                if showingDeleteButton {
                    Section {
                        Button("Delete".localized()) { self.deleteItemTapped() }.foregroundColor(.red)
                    }
                }
            }
            
        }
        .sheet(isPresented: $showingImagePicker) {
            ImagePickerView(selectedImage: self.$viewModel.photo, newSize: CGSize(width: 100, height: 100))
        }
        .alert(isPresented: $showingDeletionConfirmation) { deletionAlert! }
    }
    
    private func deleteItemTapped() {
        let deleteButton = Alert.Button.destructive(Text("Delete".localized())) { self.deleteOwner() }
        deletionAlert = Alert(
            title: Text("Confirmation".localized()),
            message: Text("If you delete this owner, all related items will be delete!".localized()),
            primaryButton: .cancel(),
            secondaryButton: deleteButton
        )
        showingDeletionConfirmation.toggle()
    }
    
    private func deleteOwner() {
        self.viewModel.delete()
        self.presentationMode.wrappedValue.dismiss()
    }
    
    private func saveButtonTapped() {
        self.viewModel.save()
        self.presentationMode.wrappedValue.dismiss()
    }
    
}
