import SwiftUI
import CoreData


class AbstractOwnerDetailsViewModel: ObservableObject {
    @Published var name = ""
    @Published var photo = UIImage(named: "person")!
    
    var userInput: [String: Any] {
        [
            "name": name,
            "photoData": photo.jpegData(compressionQuality: 1.0)!
        ]
    }
    let moc: NSManagedObjectContext
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext) {
        self.moc = moc
    }
    
    func save() {
        fatalError("Implement This!")
    }
    
    func delete() {
        fatalError("Implement This!")
    }
    
}


class AddOwnerViewModel: AbstractOwnerDetailsViewModel {
    
    override func save() {
        moc.createOwner(with: userInput)
    }
    
}


class EditOwnerViewModel: AbstractOwnerDetailsViewModel {
    var ownerToEdit: OwnerModel
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext, owner: OwnerModel) {
        ownerToEdit = owner
        super.init(moc: moc)
        name = owner.name
        photo = UIImage(data: owner.photo) ?? UIImage(systemName: "camera")!
    }
    
    override func save() {
        moc.update(ownerToEdit, with: userInput)
    }
    
    override func delete() {
        moc.delete(ownerToEdit)
    }
    
}
