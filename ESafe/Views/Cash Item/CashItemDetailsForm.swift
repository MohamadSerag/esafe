import SwiftUI


struct CashItemDetailsForm: View {
    @ObservedObject var viewModel: AbstractCashItemDetailsVM
    @Environment(\.presentationMode) var presentationMode
    var title: String
    
    private var validUserInput: Bool {
        viewModel.desc.count > 0 && Double(viewModel.amount) != nil && viewModel.currencyNames.count > 0 && viewModel.locationNames.count > 0
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Description", text: $viewModel.desc)
                    TextField("Amount", text: $viewModel.amount).keyboardType(.numbersAndPunctuation)
                }
                Section {
                    Picker("Currency", selection: $viewModel.selectedCurrencyIndex) {
                        ForEach(0 ..< viewModel.currencyNames.count, id: \.self) {
                            Text(self.viewModel.currencyNames[$0])
                        }
                    }
                    Picker("Location", selection: $viewModel.selectedLocationIndex) {
                        ForEach(0 ..< viewModel.locationNames.count, id: \.self) {
                            Text(self.viewModel.locationNames[$0])
                        }
                    }
                }
            }
            .navigationBarTitle("\(title)", displayMode: .inline)
            .navigationBarItems(
                trailing: Button(action: { self.saveBtnTapped() }, label: { Text("Save") }).disabled(!self.validUserInput)
            )
        }
    }
    
    private func saveBtnTapped() {
        viewModel.save()
        presentationMode.wrappedValue.dismiss()
    }
    
}
