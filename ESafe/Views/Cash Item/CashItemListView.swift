import SwiftUI
import CoreData


struct CashItemListView<ViewModel: ItemListVM<CashItemModel>>: View {
    let viewModel: ViewModel
    @Environment(\.editMode) var editMode
    
    @State private var showingEditView = false
    @State private var showingAddView = false
    private var addView: some View {
        CashItemDetailsForm(viewModel: AddCashItemVM(owner: viewModel.owner), title: "Add Cash")
    }
    private var trailingButtons: some View {
        HStack(spacing: 18) {
            Button(action: { showingAddView.toggle() }, label: { plusLabel })
                .disabled(editMode!.wrappedValue.isEditing)
            EditButton()
        }
    }
    
    var body: some View {
        Group {
            if viewModel.itemsCount == 0 {
                placeholderView(title: "No Cash Yet!", subTitle: "Tap the add button up right.")
            } else {
                
                ZStack {
                    
                    List(viewModel.sectionNames, id: \.self) { loc in
                        Section(header: sectionHeader(from: loc)) {
                            ForEach(viewModel.groupedItems[loc]!) { cash in
                                CashItemCell(cashItem: cash).padding(.vertical, 3)
                            }
                            .onDelete { viewModel.delete(at: $0, inSection: loc) }
                        }
                    }
                    .listStyle(SidebarListStyle())
                    
                    VStack {
                        Spacer()
                        SummaryView(
                            itemsCount: viewModel.itemsCount,
                            currencySymbol: viewModel.currencySymbol,
                            itemsTotal: viewModel.itemsTotal
                        )
                        .padding(.bottom)
                    }
                    
                }
            }
        }
        .sheet(isPresented: $showingAddView) { addView }
        .navigationBarTitle(viewModel.navBarTitle, displayMode: .inline)
        .navigationBarItems(trailing: trailingButtons)
    }
    
    private func sectionHeader(from key: String) -> some View {
        let count = viewModel.groupedItems[key]!.count
        let total = viewModel.groupedItems[key]!.map { $0.egpTotal }.reduce(0, +)
        return HStack {
            Text(key)
            Text("(\(count)) (\(viewModel.currencySymbol)\(total, specifier: "%g"))")
                .foregroundColor(.secondary)
                .font(.footnote)
        }
    }
    
}


struct CashItemCell: View {
    @ObservedObject var cashItem: CashItemModel
    @State private var showingEditView = false
    
    var body: some View {
        HStack(spacing: 10) {
            ZStack {
                Circle()
                    .frame(width: 40, height: 40)
                    .foregroundColor(Color(cashItem.currency.colorName))
                Text(cashItem.currency.code)
                    .font(.footnote)
                    .foregroundColor(Color(#colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)))
            }
            VStack(alignment: .leading) {
                HStack {
                    Text(cashItem.desc)
                        .font(.headline)
                        .foregroundColor(.primary)
                    Spacer()
                    Text("\(cashItem.currency.symbol)\(cashItem.amount, specifier: "%g")")
                        .applyEGPTotalModifiers()
                }
                Text("\("Updated on:".localized()) \(DateFormatter.df.string(from: cashItem.updatedOn))".localized())
                    .font(.subheadline)
                    .foregroundColor(.secondary)
            }
        }
        .contextMenu {
            ContextMenuEditButton(showingEditView: $showingEditView)
        }
        .sheet(isPresented: $showingEditView) { CashItemDetailsForm(viewModel: EditCashItemVM(item: cashItem), title: "Edit Cash") }
    }
    
}
