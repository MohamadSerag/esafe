import SwiftUI
import CoreData


class AbstractCashItemDetailsVM: ObservableObject {
    @Published var desc = ""
    @Published var amount = ""
    @Published var selectedCurrencyIndex = 0
    @Published var selectedLocationIndex = 0
    
    var currencyNames: [String] {
        moc.fetchCashCurrencies().map { $0.code }
    }
    var locationNames: [String] {
        moc.fetchCashLocations().map { $0.name }
    }
    var userInput: [String: Any] {
        [
            "desc": desc,
            "amount": Double(amount)!,
            "updatedOn": Date(),
            "currency": moc.fetchCashCurrencies()[selectedCurrencyIndex],
            "location": moc.fetchCashLocations()[selectedLocationIndex]
        ]
    }
    let moc: NSManagedObjectContext
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext) {
        self.moc = moc
    }
    
    func save() {
        fatalError("Implement This!")
    }
    
    func delete() {
        fatalError("Implement This!")
    }
    
}


class AddCashItemVM: AbstractCashItemDetailsVM {
    let owner: OwnerModel

    init(moc: NSManagedObjectContext = UIApplication.viewContext, owner: OwnerModel) {
        self.owner = owner
        super.init(moc: moc)
    }

    override func save() {
        var info = userInput
        info["owner"] = owner
        moc.createCashItem(with: info)
    }

}


class EditCashItemVM: AbstractCashItemDetailsVM {
    var itemToEdit: CashItemModel

    init(moc: NSManagedObjectContext = UIApplication.viewContext, item: CashItemModel) {
        itemToEdit = item
        super.init(moc: moc)
        desc = item.desc
        amount = String(format: "%g", item.amount)
        selectedCurrencyIndex = moc.fetchCashCurrencies().firstIndex(of: item.currency) ?? 0
        selectedLocationIndex = moc.fetchCashLocations().firstIndex(of: item.location) ?? 0
        
    }

    override func save() {
        moc.update(itemToEdit, with: userInput)
    }

    override func delete() {
        moc.delete(itemToEdit)
    }

}

