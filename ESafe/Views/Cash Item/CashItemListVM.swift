import Combine
import CoreData
import SwiftUI


class CashItemListVM: ItemListVM {
    let owner: OwnerModel
    var groupedItems: [String: [CashItemModel]] {
        Dictionary(grouping: owner.cashItems) { $0.location.name }
    }
    var navBarTitle: String { "\(owner.name)'s Cash" }
    // Summary view properties
    var itemsCount: Int { owner.cashItemSet.count }
    var currencySymbol: String { moc.defaultCurrencySymbol }
    var itemsTotal: Double { owner.totalCashAmount }
    
    private let moc: NSManagedObjectContext
    
    init(owner: OwnerModel, moc: NSManagedObjectContext = UIApplication.viewContext) {
        self.owner = owner
        self.moc = moc
    }
    
    func delete(at indexSet: IndexSet, inSection name: String) {
        moc.delete(groupedItems[name]![indexSet.first!])
    }
}

