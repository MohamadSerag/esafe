import Foundation

protocol ItemListVM<DataModelType> {
    associatedtype DataModelType
    
    var owner: OwnerModel { get }
    var groupedItems: [String: [DataModelType]] { get }
    var sectionNames: [String] { get }
    var navBarTitle: String { get }
    var currencySymbol: String { get }
    var itemsCount: Int { get }
    var itemsTotal: Double { get }
    
    func delete(at indexSet: IndexSet, inSection name: String)
}

extension ItemListVM {
    var sectionNames: [String] { groupedItems.sortedKeys }
}
