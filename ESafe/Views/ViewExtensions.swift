import SwiftUI


struct ContextMenuEditButton: View {
    @Binding var showingEditView: Bool
    
    var body: some View {
        Button(action: { self.showingEditView.toggle() }) {
            HStack {
                Text("Edit".localized()).imageScale(.small)
                Image(systemName: "pencil.circle")
            }
        }
    }
}


extension View {
    var welcomeView: some View {
        VStack(alignment: .center) {
            VStack(alignment: .center, spacing: 10) {
                Image("lock")                
                Text("Welcome to")
                    .font(.largeTitle)
                Text("ESafe")
                    .font(.largeTitle)
                    .fontWeight(.heavy)
                    .foregroundColor(.blue)
                Text("loading app default data, this process could take a while depending on previously stored data and internet connection, please wait...")
                    .foregroundColor(.secondary)
                    .multilineTextAlignment(.center)
                ActivityIndicator()
            }
            .offset(y: 100)
            Spacer()
        }
    }
    var plusLabel: some View {
        Image(systemName: "plus.circle").imageScale(.large)
    }
    
    func placeholderView(title: String, subTitle: String) -> some View {
        VStack(spacing: 10) {
            Text(title)
                .font(.title)
                .fontWeight(.bold)
                .foregroundColor(.secondary)
            Text(subTitle)
                .font(.subheadline)
                .foregroundColor(.secondary)
        }
    }
    
    func addTabItem(imageName: String, text: String, tag: Int) -> some View {
        tabItem {
            VStack {
                Image(systemName: imageName).imageScale(.large).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                Text(text)
            }
        }
        .tag(tag)
    }
    
    func eraseToAnyView() -> AnyView {
        AnyView(self)
    }
    
}


extension Image {
    func applyOwnerModifiers(width: CGFloat = 120, height: CGFloat = 120) -> some View {
        self
            .resizable()
            .scaledToFill()
            .frame(width: width, height: height)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.primary, lineWidth: 2))
            .shadow(radius: 5)
    }
    
}


extension Text {
    func applyEGPTotalModifiers() -> some View {
        self
            .font(.footnote)
            .foregroundColor(Color.secondary)
            .padding(.horizontal, 5)
            .overlay(Capsule().stroke(Color.secondary, lineWidth: 1))
    }
    
}


struct SummaryView: View {
    let itemsCount: Int
    let currencySymbol: String
    let itemsTotal: Double
    
    var body: some View {
        Text("Items: \(itemsCount) - Total: \(NSNumber(value: itemsTotal), formatter: formatter())")
            .padding(.horizontal, 5)
            .background(Color("summary-view-background"))
            .font(.subheadline)
            .foregroundColor(.secondary)
            .cornerRadius(8)
            .shadow(radius: 5)
    }
    
    private func formatter() -> NumberFormatter  {
       let f = NumberFormatter()
        f.currencySymbol = currencySymbol
        f.maximumFractionDigits = 0
        f.numberStyle = .currency
        return f
    }
    
}


struct ActivityIndicator: UIViewRepresentable {
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        let ind = UIActivityIndicatorView(style: .large)
        ind.startAnimating()
        return ind
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
    }
    
}
