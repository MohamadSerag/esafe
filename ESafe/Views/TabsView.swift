import SwiftUI


struct TabsView: View {
    @FetchRequest(fetchRequest: NesabModel.sortedFetchRequest()) var nesabs: FetchedResults<NesabModel>
        
    var body: some View {
        if nesabs.count == 0 {
            return welcomeView.eraseToAnyView()
        } else {
            return TabView {
                OwnerListView().addTabItem(imageName: "rectangle.stack.person.crop.fill", text: "Owners".localized(), tag: 0)
                SettingsView().addTabItem(imageName: "gear", text: "Settings".localized(), tag: 1)
            }
            .eraseToAnyView()
        }
    }
    
}
