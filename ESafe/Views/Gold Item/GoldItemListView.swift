import SwiftUI
import CoreData

struct GoldItemListView<ViewModel: ItemListVM<GoldItemModel>>: View {
    let viewModel: ViewModel
    @Environment(\.editMode) var editMode
    
    @State private var showingAddView = false
    private var addView: some View {
        NavigationView {
            GoldItemDetailsForm(viewModel: AddGoldItemVM(owner: viewModel.owner), showingOwnerPicker: false)
                .navigationBarTitle("Add Jewelry Piece", displayMode: .inline)
        }
    }
    
    var body: some View {
        Group {
            if viewModel.itemsCount == 0 {
                placeholderView(title: "No Gold Yet!", subTitle: "Tap the add button up right.")
            } else {
                
                ZStack {
                    
                    List(viewModel.sectionNames, id: \.self) { karatName in
                        Section(header: sectionHeader(from: karatName)) {
                            ForEach(viewModel.groupedItems[karatName]!) { goldItem in
                                NavigationLink(destination: editView(for: goldItem)) {
                                    GoldItemCell(goldItem: goldItem, defaultCurrencySymbol: viewModel.currencySymbol).padding(.vertical, 5)
                                }
                            }
                            .onDelete { viewModel.delete(at: $0, inSection: karatName) }
                        }
                    }
                    .listStyle(SidebarListStyle())
                    
                    VStack {
                        Spacer()
                        SummaryView(
                            itemsCount: viewModel.itemsCount,
                            currencySymbol: viewModel.currencySymbol,
                            itemsTotal: viewModel.itemsTotal
                        ).padding(.bottom)
                    }
                    
                }
            }
        }
        .sheet(isPresented: $showingAddView) { addView }
        .navigationBarTitle(viewModel.navBarTitle, displayMode: .inline)
        .navigationBarItems(
            trailing: Button(action: { showingAddView.toggle() }, label: { plusLabel })
        )
    }
    
    private func editView(for goldItem: GoldItemModel) -> some View {
        GoldItemDetailsForm(viewModel: EditGoldItemVM(item: goldItem)).navigationBarTitle("Details".localized())
    }
    
    private func sectionHeader(from key: String) -> some View {
        let count = viewModel.groupedItems[key]!.count
        let total = viewModel.groupedItems[key]!.map { $0.egpTotal }.reduce(0, +)
        let karatStr = Int(key) != nil ? "\(key)\("K")" : key
        return HStack {
            Text(karatStr)
            Text("(\(count)) (\(viewModel.currencySymbol)\(total, specifier: "%g"))")
                .foregroundColor(.secondary)
                .font(.footnote)
        }
        
        let lhs = AnyView(
            HStack {
                Text(karatStr)
                Text("(\(count)) (\(moc.defaultCurrencySymbol)\(total, specifier: "%g"))")
                    .foregroundColor(.secondary)
                    .font(.footnote)
            }
        )
        return sectionHeaderManager.header(key: key, lhs: lhs)
    }
    
}


struct GoldItemCell: View {
    @ObservedObject var goldItem: GoldItemModel
    let defaultCurrencySymbol: String
    
    var body: some View {
        HStack(spacing: 15) {
            Image(uiImage: UIImage(data: goldItem.thumbnail)!)
                .resizable()
                .scaledToFill()
                .frame(width: 60, height: 70)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.primary, lineWidth: 2))
            
            VStack(alignment: .leading, spacing: 8) {
                HStack {
                    Text(goldItem.desc)
                        .font(.headline)
                    Spacer()
                    Text("\(defaultCurrencySymbol)\(Int(goldItem.egpTotal))")
                        .applyEGPTotalModifiers()
                }
                Text("\("Purchased on:".localized()) \(DateFormatter.df.string(from: goldItem.purchaseDate))".localized())
                    .font(.subheadline)
                    .foregroundColor(.secondary)
            }
        }
    }
    
}
