import SwiftUI


struct GoldItemDetailsForm: View {
    @ObservedObject var viewModel: AbstractGoldItemDetailsVM 
    @Environment(\.presentationMode) var presentationMode
    var showingOwnerPicker = true
    
    @State private var showingImagePicker = false
    @ObservedObject private var keyboard = KeyboardResponder()
    private let imageHeight: CGFloat = 215
    private var validUserInput: Bool {
        viewModel.desc.count > 0 && Double(viewModel.weight) != nil && viewModel.karatNames.count > 0
    }
    
    var body: some View {
        Form {
            Section {
                HStack(alignment: .center) {
                    Spacer()
                    Image(uiImage: viewModel.image)
                        .resizable()
                        .scaledToFit()
                        .cornerRadius(10)
                        .shadow(radius: 5, y:5)
                        .onTapGesture { self.showingImagePicker.toggle() }
                    Spacer()
                }
                .frame(height: imageHeight)
                
                TextField("Weight (or quantity in case of Coin)".localized(), text: $viewModel.weight).keyboardType(.decimalPad)
                TextField("Description".localized(), text: $viewModel.desc)
                TextField("Note".localized(), text: $viewModel.note)
            }
            Section {
                Picker("Karat".localized(), selection: $viewModel.selectedKaratIndex) {
                    ForEach(0 ..< viewModel.karats.count, id: \.self) {
                        KaratPickerCell(karat: self.viewModel.karats[$0])
                    }
                }
                if showingOwnerPicker {
                    Picker("Owner".localized(), selection: $viewModel.selectedOwnerIndex) {
                        ForEach(0 ..< viewModel.ownerNames.count, id: \.self) {
                            Text(self.viewModel.ownerNames[$0])
                        }
                    }
                }
            }
            Section {
                DatePicker(selection: $viewModel.purchaseDate, displayedComponents: .date, label: { Text("Purchase Date".localized()) })
                Toggle(isOn: $viewModel.sold, label: { Text("Sold".localized()) })
                if viewModel.sold {
                    DatePicker(selection: $viewModel.sellDate, displayedComponents: .date,label: { Text("Sell Date") })
                }
            }
        }
        .offset(y: keyboard.currentHeight)//.animation(.easeInOut(duration: 0.5))
        .navigationBarItems(
            trailing: Button(action: { self.saveBtnTapped() }, label: { Text("Save".localized()) }).disabled(!self.validUserInput)
        )
            .sheet(isPresented: $showingImagePicker) {
                ImagePickerView(selectedImage: self.$viewModel.image, newSize: CGSize(width: 400, height: self.imageHeight))
        }
    }
    
    private func saveBtnTapped() {
        self.viewModel.save()
        self.presentationMode.wrappedValue.dismiss()
    }
    
}


struct KaratPickerCell: View {
    let karat: KaratModel
    
    var body: some View {
        HStack {
            Text(Int(karat.name) != nil ? "\(karat.name)K" : karat.name)
            Text("1 \(karat.name == "Coin" ? "coin" : "gram") = EGP\(karat.egpRate, specifier: "%g")")
                .font(.subheadline)
                .foregroundColor(.secondary)
        }
    }
}


class KeyboardResponder: ObservableObject {
    @Published private(set) var currentHeight: CGFloat = 0
    private var notificationCenter: NotificationCenter
    
    init(center: NotificationCenter = .default) {
        notificationCenter = center
        notificationCenter.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    @objc func keyBoardWillShow(notification: Notification) {
        currentHeight = -45
    }
    
    @objc func keyBoardWillHide(notification: Notification) {
        currentHeight = 0
    }
    
}
