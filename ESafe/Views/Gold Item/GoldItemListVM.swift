import Combine
import CoreData
import SwiftUI


class GoldItemListVM: ItemListVM {
    let owner: OwnerModel
    
    var groupedItems: [String: [GoldItemModel]] {
        let sorted = self.owner.goldItems.sorted {
            if Calendar.current.isDate($0.purchaseDate, inSameDayAs: $1.purchaseDate) {
                return $0.egpTotal > $1.egpTotal
            } else {
                return $0.purchaseDate > $1.purchaseDate
            }
        }
        return Dictionary(grouping: sorted) { $0.karat.name }
    }
    var navBarTitle: String { "\(owner.name)'s Jewelry" }
    
    // Summary view properties
    var itemsCount: Int { owner.goldItemSet.count }
    var currencySymbol: String { moc.defaultCurrencySymbol }
    var itemsTotal: Double { owner.totalGoldPrice + owner.totalSilverPrice }
    
    private let moc: NSManagedObjectContext
    
    init(owner: OwnerModel, moc: NSManagedObjectContext = UIApplication.viewContext) {
        self.owner = owner
        self.moc = moc
    }
    
    func delete(at indexSet: IndexSet, inSection name: String) {
        moc.delete(groupedItems[name]![indexSet.first!])
    }
    
}
