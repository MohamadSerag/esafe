import SwiftUI
import CoreData


class AbstractGoldItemDetailsVM: ObservableObject {
    @Published var image = UIImage(named: "camera")!
    @Published var desc = ""
    @Published var weight = ""
    @Published var purchaseDate = Date()
    @Published var sold = false
    @Published var sellDate = Date()
    @Published var note = ""
    @Published var selectedKaratIndex = 0
    @Published var selectedOwnerIndex = 0
    
    var karatNames: [String] {
        moc.fetchKarats().map { $0.name }
    }
    var karats: [KaratModel] {
        moc.fetchKarats()
    }
    var ownerNames: [String] {
        moc.fetchOwners().map { $0.name }
    }
    var userInput: [String: Any] {
        [
            "image": image.jpegData(compressionQuality: 1.0)!,
            "thumbnail": image.resize(to: CGSize(width: 60, height: 70)).jpegData(compressionQuality: 1.0)!,
            "desc": desc,
            "weight": weight,
            "purchaseDate": purchaseDate,
            "sold": sold,
            "sellDate": sellDate,
            "note": note,
            "karat": moc.fetchKarats()[selectedKaratIndex],
            "owner": moc.fetchOwners()[selectedOwnerIndex]
        ]
    }
    let moc: NSManagedObjectContext
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext) {
        self.moc = moc
    }
    
    func save() {
        fatalError("Implement This!")
    }
    
    func delete() {
        fatalError("Implement This!")
    }
    
}


class AddGoldItemVM: AbstractGoldItemDetailsVM {
    let owner: OwnerModel
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext, owner: OwnerModel) {
        self.owner = owner
        super.init(moc: moc)
        selectedOwnerIndex = moc.fetchOwners().firstIndex(of: owner)!
    }
    
    override func save() {
        moc.createGoldItem(with: userInput)
    }
    
}


class EditGoldItemVM: AbstractGoldItemDetailsVM {
    var itemToEdit: GoldItemModel
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext, item: GoldItemModel) {
        itemToEdit = item
        super.init(moc: moc)
        image = UIImage(data: item.image.data)!
        desc = item.desc
        weight = String(format: "%g", item.weight)
        purchaseDate = item.purchaseDate
        sold = item.sold
        if item.sold { sellDate = item.sellDate! }
        note = item.note
        selectedKaratIndex = moc.fetchKarats().firstIndex(of: item.karat) ?? 0
        selectedOwnerIndex = moc.fetchOwners().firstIndex(of: item.owner) ?? 0
    }
    
    override func save() {
        moc.update(itemToEdit, with: userInput)
    }
    
    override func delete() {
        moc.delete(itemToEdit)
    }
    
}
