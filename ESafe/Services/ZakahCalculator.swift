import SwiftUI
import CoreData


class ZakahCalculator {
    private var nesab: Double!
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext) {
        let nKarat = moc.fetchNesabs().first { $0.isActive }!
        nesab = (nKarat.grams * nKarat.karat.egpRate).rounded()
    }
    
    private func reachesNesab(itemsTotal: Double, supplementCash: Double) -> (reaches: Bool, excessCash: Double) {
        var reaches = false
        var excessCash = supplementCash
        
        if itemsTotal >= nesab {
            reaches = true
        } else if (itemsTotal + supplementCash) >= nesab {
            reaches = true
            excessCash = supplementCash - (nesab - itemsTotal)
        }
        return (reaches, excessCash)
    }
    
    func zakah(for owner: OwnerModel) -> Double {
        let goldCheckResult = reachesNesab(itemsTotal: owner.totalGoldPrice, supplementCash: owner.totalCashAmount)
        let silverCheckResult = reachesNesab(itemsTotal: owner.totalSilverPrice, supplementCash: goldCheckResult.excessCash)
        
        var zakah: Double = 0
        if goldCheckResult.reaches { zakah += (owner.totalGoldPrice * 0.025) }
        if silverCheckResult.reaches { zakah += (owner.totalSilverPrice * 0.025) }
        if goldCheckResult.reaches || silverCheckResult.reaches { zakah += (owner.totalCashAmount * 0.025) }
        return zakah.rounded()
    }
    
}
