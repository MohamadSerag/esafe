import CoreData


extension NSManagedObjectContext {
    var isEmpty: Bool { !willReturnItems(req: NesabModel.sortedFetchRequest()) }
    var karatsInStore: Bool { willReturnItems(req: KaratModel.sortedFetchRequest()) }
    var currenciesInStore: Bool { willReturnItems(req: CashCurrencyModel.sortedFetchRequest()) }
    var locationsInStore: Bool { willReturnItems(req: CashLocationModel.sortedFetchRequest()) }
    var nesabsInStore: Bool { willReturnItems(req: NesabModel.sortedFetchRequest()) }
    var defaultCurrencySymbol: String { fetchCashCurrencies().first { $0.isDefault }?.symbol ?? "" }
    
    private func willReturnItems<T:NSManagedObject>(req: NSFetchRequest<T>) -> Bool {
        if let count = try? count(for: req), count > 0 {
            return true
        }
        return false
    }
    
    func createOwner(with info: [String: Any]) {
        let owner = OwnerModel(context: self)
        update(owner, with: info)
    }
    
    func update(_ owner: OwnerModel, with info: [String: Any]) {
        owner.objectWillChange.send()
        owner.name = info["name"]! as! String
        owner.photo = info["photoData"]! as! Data
    }
    
    func createGoldItem(with info: [String: Any]) {
        let goldItem = GoldItemModel(context: self)
        let imageAsset = ImageAssetModel(context: self)
        goldItem.image = imageAsset
        update(goldItem, with: info)
    }
    
    func update(_ goldItem: GoldItemModel, with info: [String: Any]) {
        goldItem.objectWillChange.send()
        goldItem.image.data = info["image"]! as! Data
        goldItem.thumbnail = info["thumbnail"]! as! Data
        goldItem.desc = info["desc"]! as! String
        goldItem.weight = Double(info["weight"]! as! String)!
        goldItem.purchaseDate = info["purchaseDate"]! as! Date
        goldItem.sellDate = (info["sold"]! as! Bool) ? info["sellDate"]! as? Date: nil
        goldItem.note = info["note"]! as! String
        goldItem.karat = info["karat"]! as! KaratModel
        if let owner = info["owner"] as? OwnerModel, owner != goldItem.owner {
            goldItem.owner = owner
        }
    }
    
    func fetchKarats() -> [KaratModel] {
        try! fetch(KaratModel.sortedFetchRequest())
    }
    
    func fetchOwners() -> [OwnerModel] {
        try! fetch(OwnerModel.sortedFetchRequest())
    }
    
    func fetchNesabs() -> [NesabModel] {
        try! fetch(NesabModel.sortedFetchRequest())
    }
    
    func createCashItem(with info: [String: Any]) {
        let item = CashItemModel(context: self)
        update(item, with: info)
    }
    
    func update(_ cashItem: CashItemModel, with info: [String: Any]) {
        cashItem.objectWillChange.send()
        cashItem.desc = info["desc"]! as! String
        cashItem.amount = info["amount"]! as! Double
        cashItem.updatedOn = Date()
        cashItem.currency = info["currency"]! as! CashCurrencyModel
        cashItem.location = info["location"]! as! CashLocationModel
        if let owner = info["owner"] as? OwnerModel, owner != cashItem.owner {
            cashItem.owner = owner
        }
    }
    
    func fetchCashCurrencies() -> [CashCurrencyModel] {
        try! fetch(CashCurrencyModel.sortedFetchRequest())
    }
    
    func fetchCashLocations() -> [CashLocationModel] {
        try! fetch(CashLocationModel.sortedFetchRequest())
    }
    
    func createCurrency(with info: [String: Any]) {
        let currency = CashCurrencyModel(context: self)
        update(currency, with: info)
    }
    
    func update(_ currency: CashCurrencyModel, with info: [String: Any]) {
        currency.objectWillChange.send()
        currency.code = info["code"]! as! String
        currency.symbol = info["symbol"]! as! String
        currency.egpRate = info["egpRate"]! as! Double
        currency.colorName = info["colorName"]! as! String
        currency.isSystem = false
    }
    
    func createLocation(with info: [String: Any]) {
        let loc = CashLocationModel(context: self)
        update(loc, with: info)
    }
    
    func update(_ loc: CashLocationModel, with info: [String: Any]) {
        loc.objectWillChange.send()
        loc.name = info["name"]! as! String
    }
    
    func createKarat(with info: [String: Any]) {
        let karat = KaratModel(context: self)
        update(karat, with: info)
    }
    
    func update(_ karat: KaratModel, with info: [String: Any]) {
        karat.objectWillChange.send()
        karat.name = info["name"]! as! String
        karat.egpRate = info["egpRate"]! as! Double
        karat.isSystem = false
    }
    
    func loadDefaultKaratsAndNesabs() {
        ["18,579", "21,675", "24,771", "Coin,5400", "Silver,8.81"].forEach {
            let k = KaratModel(context: self)
            k.name = $0.components(separatedBy: ",")[0]
            k.egpRate = Double($0.components(separatedBy: ",")[1])!
            k.isSystem = true
        }
        loadDefaultNesabs()
    }
    
    func loadDefaultCurrencies() {
           ["EGP,1,EGP,c1", "USD,16.1,$,c10"].forEach {
                       let curr = CashCurrencyModel(context: self)
                       curr.code = $0.components(separatedBy: ",")[0]
                       curr.egpRate = Double($0.components(separatedBy: ",")[1])!
                       curr.symbol = $0.components(separatedBy: ",")[2]
                       curr.colorName = $0.components(separatedBy: ",")[3]
                       curr.isDefault = $0.components(separatedBy: ",")[0] == "EGP" ? true : false
                       curr.isSystem = true
                   }
       }
       
    func loadDefaultLocations() {
        ["Bank", "Home", "Loanee"].forEach {
            let loc = CashLocationModel(context: self)
            loc.isSystem = true
            loc.name = $0
        }
    }
    
    func loadDefaultNesabs() {
        let gold = NesabModel(context: self)
        gold.grams = 85
        gold.karat = fetchKarats().first { $0.name == "24" }!
        gold.name = "Gold"

        let silver = NesabModel(context: self)
        silver.name = "Silver"
        silver.grams = 595
        silver.isActive = true

        if let silverKarat = fetchKarats().first(where: { $0.name == "Silver" }) {
            silver.karat = silverKarat
        } else {
            let k = KaratModel(context: self)
            k.name = "Silver"
            k.egpRate = 8.81
            k.isSystem = true
            silver.karat = k
        }
    }
    
}
