import CoreData
import CloudKit
import SwiftUI


extension NSPersistentCloudKitContainer {
    private var defaultEntryLoaders: [AbstractDefaultEntriesLoader] {
        [
            KaratDefaultEntriesLoader(entityName: "CD_KaratModel"),
            CashCurrencyDefaultEntriesLoader(entityName: "CD_CashCurrencyModel"),
            CashLocationDefaultEntriesLoader(entityName: "CD_CashLocationModel"),
        ]
    }
    
    func setupLocalStoreIfNeeded() {
        defaultEntryLoaders.forEach { $0.loadDefaultEntriesIfNeeded() }
        if viewContext.hasChanges { try? viewContext.save() }
    }
    
}


fileprivate class AbstractDefaultEntriesLoader {
    fileprivate let entityName: String
    fileprivate let moc: NSManagedObjectContext
    fileprivate let ckDatabase: CKDatabase
    fileprivate var inStore: Bool { false }
    
    init(entityName: String, moc: NSManagedObjectContext = UIApplication.viewContext, ckDatabase: CKDatabase = CKContainer.default().privateCloudDatabase) {
        self.entityName = entityName
        self.moc = moc
        self.ckDatabase = ckDatabase
    }
    
    fileprivate func loadDefaultEntries() {
        fatalError("Must Implement!")
    }
    
    fileprivate func checkCloudStore(onEmptyCompletion: @escaping () -> ()) {
        let op = CKQueryOperation(query: CKQuery(recordType: entityName, predicate: NSPredicate(value: true)))
        op.zoneID = CKRecordZone(zoneName: "com.apple.coredata.cloudkit.zone").zoneID
        op.qualityOfService = .userInitiated
        op.resultsLimit = 1
        
        var fetched: CKRecord?
        op.recordMatchedBlock = {(_, result) in
            switch result {
            case .failure:
                print("fail")
            case .success(let record):
                fetched = record
            }
        }
        op.completionBlock = { if fetched == nil { onEmptyCompletion() } }
        ckDatabase.add(op)
    }
    
    func loadDefaultEntriesIfNeeded() {
        if inStore == false {
            checkCloudStore { DispatchQueue.main.async { self.loadDefaultEntries() } }
        }
    }
    
}


fileprivate class KaratDefaultEntriesLoader: AbstractDefaultEntriesLoader {
    override var inStore: Bool { moc.karatsInStore }
    
    private func loadNesabs() {
        let gold = NesabModel(context: moc)
        gold.grams = 85
        gold.karat = moc.fetchKarats().first { $0.name == "24" }!
        gold.name = "Gold"
        
        let silver = NesabModel(context: moc)
        silver.name = "Silver"
        silver.grams = 595
        silver.isActive = true
        silver.karat = moc.fetchKarats().first { $0.name == "Silver" }!
    }
    
    override func loadDefaultEntries() {
        ["18,579", "21,675", "24,771", "Coin,5400", "Silver,8.81"].forEach {
            let k = KaratModel(context: moc)
            k.name = $0.components(separatedBy: ",")[0]
            k.egpRate = Double($0.components(separatedBy: ",")[1])!
            k.isSystem = true
        }
        loadNesabs()
    }
    
    override func loadDefaultEntriesIfNeeded() {
        if inStore == false {
            checkCloudStore { DispatchQueue.main.async { self.loadDefaultEntries() } }
        } else if moc.nesabsInStore == false {
            loadNesabs()
        }
    }
    
}


fileprivate class CashCurrencyDefaultEntriesLoader: AbstractDefaultEntriesLoader {
    override var inStore: Bool { moc.currenciesInStore }
    
    override func loadDefaultEntries() {
        ["EGP,1,EGP,c1", "USD,16.1,$,c10"].forEach {
            let curr = CashCurrencyModel(context: moc)
            curr.code = $0.components(separatedBy: ",")[0]
            curr.egpRate = Double($0.components(separatedBy: ",")[1])!
            curr.symbol = $0.components(separatedBy: ",")[2]
            curr.colorName = $0.components(separatedBy: ",")[3]
            curr.isDefault = $0.components(separatedBy: ",")[0] == "EGP" ? true : false
            curr.isSystem = true
        }
    }
    
}


fileprivate class CashLocationDefaultEntriesLoader: AbstractDefaultEntriesLoader  {
    override var inStore: Bool { moc.locationsInStore }
    
    override func loadDefaultEntries() {
        ["Bank", "Home", "Loanee"].forEach {
            let loc = CashLocationModel(context: moc)
            loc.isSystem = true
            loc.name = $0
        }
    }
    
}
