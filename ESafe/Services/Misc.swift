import SwiftUI
import CloudKit
import CoreData


extension String: Identifiable {
    public var id: String { self }
    
    func localized() -> String {
        NSLocalizedString(self, tableName: "Localizable", bundle: Bundle.main, value: self, comment: self)
    }
}


extension Dictionary where Dictionary.Key: Comparable {
    var sortedKeys: [Key] { keys.sorted() }
}


extension UIImage {
    func resize(to requiredSize: CGSize) -> UIImage {
        let originalSize = size
        let widthRatio = requiredSize.width / originalSize.width
        let heightRatio = requiredSize.height / originalSize.height
        
        let ratio = min(widthRatio, heightRatio)
        let newSize = CGSize(width: originalSize.width * ratio, height: originalSize.height * ratio)
        // preparing rect for new image size
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
}


extension Double {
    var stringWithComma: String {
        let nf = NumberFormatter()
        nf.groupingSeparator = ","
        nf.numberStyle = .decimal
        return nf.string(from: self as NSNumber)!
    }
    
}


extension DateFormatter {
    static var df: DateFormatter {
        let df = DateFormatter()
        df.dateStyle = .medium
        df.timeStyle = .none
        return df
    }
    
}
