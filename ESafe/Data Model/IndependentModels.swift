import CoreData
import SwiftUI


enum RemovabilityState: String {
    case unremovable
    case removableWithCascade
    case removable
}


protocol ListItem {
    var hasChildren: Bool { get }
    var isSystem: Bool { get set }
    var removabilityState: RemovabilityState { get }
}


extension ListItem {
    var removabilityState: RemovabilityState {
        if isSystem {
            return .unremovable
        } else if hasChildren {
            return .removableWithCascade
        } else {
            return .removable
        }
    }
    
}


class CashCurrencyModel: NSManagedObject, Identifiable, ListItem {
    class func sortedFetchRequest() -> NSFetchRequest<CashCurrencyModel> {
        let req = fetchRequest() as! NSFetchRequest<CashCurrencyModel>
        req.sortDescriptors = [NSSortDescriptor(keyPath: \CashCurrencyModel.code, ascending: true)]
        return req
    }
    static var availableColor: [String: Color] = {
        var dict = [String: Color]()
        for i in 1 ... 12 {
            dict["c\(i)"] = Color("c\(i)")
        }
        return dict
    }()

    @NSManaged var code: String
    @NSManaged var symbol: String
    @NSManaged var colorName: String
    @NSManaged var egpRate: Double
    @NSManaged var isDefault: Bool
    @NSManaged var isSystem: Bool
    @NSManaged var cashItemSet: NSSet
    
    var hasChildren: Bool { cashItemSet.count > 0 }
    var cashItems: [CashItemModel] {
        cashItemSet.map { $0 as! CashItemModel }.sorted { (item1, item2) -> Bool in
            if Calendar.current.isDate(item1.updatedOn, inSameDayAs: item2.updatedOn) {
                return item1.amount > item2.amount
            } else {
                return item1.updatedOn > item2.updatedOn
            }
        }
    }
    
}


class CashLocationModel: NSManagedObject, Identifiable, ListItem {
    class func sortedFetchRequest() -> NSFetchRequest<CashLocationModel> {
        let req = fetchRequest() as! NSFetchRequest<CashLocationModel>
        req.sortDescriptors = [NSSortDescriptor(keyPath: \CashLocationModel.name, ascending: true)]
        return req
    }

    @NSManaged var name: String
    @NSManaged var isSystem: Bool
    @NSManaged var cashItemSet: NSSet
    
    private var cashItems: [CashItemModel] {
        cashItemSet.map { $0 as! CashItemModel }.sorted { (item1, item2) -> Bool in
            if Calendar.current.isDate(item1.updatedOn, inSameDayAs: item2.updatedOn) {
                return item1.amount > item2.amount
            } else {
                return item1.updatedOn > item2.updatedOn
            }
        }
    }
    var id: String { name }
    var hasChildren: Bool { cashItemSet.count > 0 }
    
    func hasCashOwned(by owner: OwnerModel) -> Bool {
        !cashItems.allSatisfy { $0.owner != owner }
    }
    
    func cashItems(for owner: OwnerModel) -> [CashItemModel] {
        cashItems.filter { $0.owner == owner }
    }
    
}


class KaratModel: NSManagedObject, Identifiable, ListItem {
    class func sortedFetchRequest() -> NSFetchRequest<KaratModel> {
        let req = fetchRequest() as! NSFetchRequest<KaratModel>
        req.sortDescriptors = [NSSortDescriptor(keyPath: \KaratModel.name, ascending: true)]
        return req
    }
    
    @NSManaged var name: String
    @NSManaged var egpRate: Double
    @NSManaged var goldItemSet: NSSet
    @NSManaged var isSystem: Bool
    @NSManaged var nesab: NesabModel?
    
    private var goldItems: [GoldItemModel] {
        goldItemSet.map { $0 as! GoldItemModel }.sorted { (item1, item2) in
            if item1.karat.name != item2.karat.name {
                return item1.karat.name < item1.karat.name
            } else {
                return item1.purchaseDate > item2.purchaseDate
            }
        }
    }

    var hasChildren: Bool { goldItemSet.count > 0 }
    
    func hasGoldOwned(by owner: OwnerModel) -> Bool {
        !goldItems.allSatisfy { $0.owner != owner }
    }
    
    func goldItems(for owner: OwnerModel) -> [GoldItemModel] {
        goldItems.filter { $0.owner == owner }
    }
    
}


class NesabModel: NSManagedObject {
    class func sortedFetchRequest() -> NSFetchRequest<NesabModel> {
        let req = fetchRequest() as! NSFetchRequest<NesabModel>
        req.sortDescriptors = [NSSortDescriptor(keyPath: \NesabModel.grams, ascending: true)]
        return req
    }

    @NSManaged var name: String
    @NSManaged var grams: Double
    @NSManaged var isActive: Bool
    @NSManaged var karat: KaratModel
}


