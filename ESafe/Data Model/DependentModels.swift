import CoreData
import SwiftUI


class OwnerModel: NSManagedObject, Identifiable {
    class func sortedFetchRequest() -> NSFetchRequest<OwnerModel> {
        let req: NSFetchRequest<OwnerModel> = NSFetchRequest(entityName: String(describing: self))
        req.sortDescriptors = [NSSortDescriptor(keyPath: \OwnerModel.name, ascending: true)]
        return req
    }
    
    @NSManaged var name: String
    @NSManaged var photo: Data
    @NSManaged var goldItemSet: NSSet
    @NSManaged var cashItemSet: NSSet
    
    var goldItems: [GoldItemModel] {
        goldItemSet.map { $0 as! GoldItemModel }
    }
    var cashItems: [CashItemModel] {
        cashItemSet.map { $0 as! CashItemModel }
    }
    var totalGoldPrice: Double {
        goldItemSet.filter { !($0 as! GoldItemModel).sold && ($0 as! GoldItemModel).karat.name != "Silver" }.map { ($0 as! GoldItemModel).egpTotal }.reduce(0, +).rounded()
    }
    var totalSilverPrice: Double {
        goldItemSet.filter { ($0 as! GoldItemModel).karat.name == "Silver" }.map { ($0 as! GoldItemModel).egpTotal }.reduce(0, +).rounded()
    }
    var totalCashAmount: Double { cashItemSet.map { ($0 as! CashItemModel).egpTotal }.reduce(0, +).rounded() }

}




class CashItemModel: NSManagedObject, Identifiable {
    class func sortedFetchRequest(using predicate: NSPredicate? = nil) -> NSFetchRequest<CashItemModel> {
        let req = fetchRequest() as! NSFetchRequest<CashItemModel>
        req.sortDescriptors = [
            NSSortDescriptor(keyPath: \CashItemModel.amount, ascending: false),
            NSSortDescriptor(keyPath: \CashItemModel.updatedOn, ascending: false)
        ]
        req.predicate = predicate
        return req
    }
    
    @NSManaged var desc: String
    @NSManaged var amount: Double
    @NSManaged var updatedOn: Date
    @NSManaged var owner: OwnerModel
    @NSManaged var currency: CashCurrencyModel
    @NSManaged var location: CashLocationModel
    
    var egpTotal: Double { amount * currency.egpRate }

}


class GoldItemModel: NSManagedObject, Identifiable {
    class func sortedFetchRequest(using predicate: NSPredicate? = nil) -> NSFetchRequest<GoldItemModel> {
        let req = fetchRequest() as! NSFetchRequest<GoldItemModel>
        req.sortDescriptors = [NSSortDescriptor(keyPath: \GoldItemModel.purchaseDate, ascending: false)]
        req.predicate = predicate
        return req
    }
    
    @NSManaged var image: ImageAssetModel
    @NSManaged var thumbnail: Data
    @NSManaged var desc: String
    @NSManaged var weight: Double
    @NSManaged var purchaseDate: Date
    @NSManaged var sellDate: Date?
    @NSManaged var note: String
    @NSManaged var owner: OwnerModel
    @NSManaged var karat: KaratModel
    
    var sold: Bool { sellDate != nil }
    var egpTotal: Double { weight * karat.egpRate }
    
}


class ImageAssetModel: NSManagedObject {
    @NSManaged var data: Data
    @NSManaged var goldItem: GoldItemModel
}
